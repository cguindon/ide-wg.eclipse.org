---
title: "About Us"
date: 2021-05-04T10:00:00-04:00
#hide_page_title: true
layout: "single"
---

The Eclipse® IDE Working Group is formed to ensure the continued sustainability, integrity, evolution and adoption of the Eclipse IDE suite of products and related technologies. In particular, it is formed to provide governance, guidance, and funding for the communities that support the delivery of the Eclipse Foundation’s flagship “Eclipse IDE” products.

This working group's website is under development.